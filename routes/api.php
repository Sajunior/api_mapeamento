<?php

use Illuminate\Http\Request;
use App\Lote;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/lotes', function () {
    //return Lote::where('gid','30720')->first();
    return response()->json([Lote::paginate(15)]);
});

Route::post('/lotesbyname', function (Request $request) {
    
    $data = $request->all();
    //dd($data["word"]);

    return response()->json([Lote::where('nomebairro', 'LIKE', $data["word"]."%")->get()]);
});

Route::get('/teste', function () {
    //return Lote::where('gid','30720')->first();
    return response()->json(["ola" => "isso ai"]);
});

Route::get('/bairros', 'Bairro2013Controller@index');
Route::post('/bairrosbyname', 'Bairro2013Controller@bairrosByName');

Route::get('/economico', 'EconomicoController@index');
Route::post('/economicobyname', 'EconomicoController@economicoByName');


